﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Specjalność_dla_Archera
{
    public partial class Form1 : Form
    {
        byte what = 0;
        double wynik0 = 0, wynik1 = 0, ogolem;
        public Form1()
        {
            InitializeComponent();
        }

        private void podsumowanie_Click(object sender, EventArgs e)
        {
            try
            {
                wynik1 = Convert.ToDouble(wynik.Text);
                switch (what)
                {     
                    case 1:
                        ogolem = Matematyka.Dodaj(wynik0, wynik1);//Matematyka.Dodaj(wynik0, wynik1).ToString();
                        break;
                    case 2:
                        ogolem = Matematyka.Odejmi(wynik0, wynik1);
                        break;
                    case 3:
                        ogolem = Matematyka.Pomnoz(wynik0, wynik1);
                        break;
                    case 4:
                        ogolem = Matematyka.Dziel(wynik0, wynik1);
                        //wynik.Text = Matematyka.Dziel(out wynik0, out wynik1).ToString();
                        break;
                    default:
                        break;
                }
                wynik0 = 0; wynik1 = 0;
                wynik.Text = ogolem.ToString();
            }
            catch (Exception ex)
            {
                wynik.Text = ex.Message;
            }
        }

        private void dodac_Click(object sender, EventArgs e)
        {
            what = 1;
            Matematyka.Czysc(wynik, ref wynik0);
        }

        private void odjac_Click(object sender, EventArgs e)
        {
            what = 2;
            Matematyka.Czysc(wynik, ref wynik0);
        }

        private void podzielic_Click(object sender, EventArgs e)
        {
            what = 4;
            Matematyka.Czysc(wynik, ref wynik0);
        }

        private void mnożenie_Click(object sender, EventArgs e)
        {
            what = 3;
            Matematyka.Czysc(wynik, ref wynik0);
        }

        private void one_Click(object sender, EventArgs e)
        {
            Matematyka.Wyniki(wynik, "1");
        }

        private void two_Click(object sender, EventArgs e)
        {
            Matematyka.Wyniki(wynik, "2");
        }

        private void three_Click(object sender, EventArgs e)
        {
            Matematyka.Wyniki(wynik, "3");
        }

        private void four_Click(object sender, EventArgs e)
        {
            Matematyka.Wyniki(wynik, "4");
        }

        private void five_Click(object sender, EventArgs e)
        {
            Matematyka.Wyniki(wynik, "5");
        }

        private void six_Click(object sender, EventArgs e)
        {
            Matematyka.Wyniki(wynik, "6");
        }

        private void seven_Click(object sender, EventArgs e)
        {
            Matematyka.Wyniki(wynik, "7");
        }

        private void eight_Click(object sender, EventArgs e)
        {
            Matematyka.Wyniki(wynik, "8");
        }

        private void nine_Click(object sender, EventArgs e)
        {
            Matematyka.Wyniki(wynik, "9");
        }

        private void zero_Click(object sender, EventArgs e)
        {
            Matematyka.Wyniki(wynik, "0");
        }

        private void clear_Click(object sender, EventArgs e)
        {
            wynik.Text = string.Empty;
            wynik0 = 0;
            wynik1 = 0;
        }
    }
}
