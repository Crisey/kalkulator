﻿using System;
using System.Text;
using System.Windows.Forms;

namespace Specjalność_dla_Archera
{
    public static class Matematyka
    {
        public static void Wyniki (TextBox txtBox, string number)
        {
            if (txtBox.TextLength < 45)
                txtBox.Text += number;
        }

        public static double Dodaj(double a, double b)
        {
            return (a + b);
        }

        public static double Odejmi(double a, double b)
        {

            return (a - b);
        }

        public static double Pomnoz(double a, double b)
        {

            return (a * b);
        }

        public static double Dziel(double a, double b)
        {
            return (a / b);
        }

        public static void Czysc(TextBox txtBox,ref double b)
        {
            b = Convert.ToDouble(txtBox.Text);
            txtBox.Text = string.Empty;
        }
    }
}
