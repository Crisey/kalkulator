﻿namespace Specjalność_dla_Archera
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.one = new System.Windows.Forms.Button();
            this.two = new System.Windows.Forms.Button();
            this.three = new System.Windows.Forms.Button();
            this.six = new System.Windows.Forms.Button();
            this.five = new System.Windows.Forms.Button();
            this.four = new System.Windows.Forms.Button();
            this.seven = new System.Windows.Forms.Button();
            this.eight = new System.Windows.Forms.Button();
            this.nine = new System.Windows.Forms.Button();
            this.dodac = new System.Windows.Forms.Button();
            this.odjac = new System.Windows.Forms.Button();
            this.podzielic = new System.Windows.Forms.Button();
            this.mnożenie = new System.Windows.Forms.Button();
            this.podsumowanie = new System.Windows.Forms.Button();
            this.wynik = new System.Windows.Forms.TextBox();
            this.zero = new System.Windows.Forms.Button();
            this.clear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // one
            // 
            this.one.Location = new System.Drawing.Point(12, 43);
            this.one.Name = "one";
            this.one.Size = new System.Drawing.Size(75, 23);
            this.one.TabIndex = 0;
            this.one.Text = "1";
            this.one.UseVisualStyleBackColor = true;
            this.one.Click += new System.EventHandler(this.one_Click);
            // 
            // two
            // 
            this.two.Location = new System.Drawing.Point(93, 43);
            this.two.Name = "two";
            this.two.Size = new System.Drawing.Size(75, 23);
            this.two.TabIndex = 1;
            this.two.Text = "2";
            this.two.UseVisualStyleBackColor = true;
            this.two.Click += new System.EventHandler(this.two_Click);
            // 
            // three
            // 
            this.three.Location = new System.Drawing.Point(174, 43);
            this.three.Name = "three";
            this.three.Size = new System.Drawing.Size(75, 23);
            this.three.TabIndex = 2;
            this.three.Text = "3";
            this.three.UseVisualStyleBackColor = true;
            this.three.Click += new System.EventHandler(this.three_Click);
            // 
            // six
            // 
            this.six.Location = new System.Drawing.Point(174, 72);
            this.six.Name = "six";
            this.six.Size = new System.Drawing.Size(75, 23);
            this.six.TabIndex = 3;
            this.six.Text = "6";
            this.six.UseVisualStyleBackColor = true;
            this.six.Click += new System.EventHandler(this.six_Click);
            // 
            // five
            // 
            this.five.Location = new System.Drawing.Point(93, 72);
            this.five.Name = "five";
            this.five.Size = new System.Drawing.Size(75, 23);
            this.five.TabIndex = 4;
            this.five.Text = "5";
            this.five.UseVisualStyleBackColor = true;
            this.five.Click += new System.EventHandler(this.five_Click);
            // 
            // four
            // 
            this.four.Location = new System.Drawing.Point(12, 72);
            this.four.Name = "four";
            this.four.Size = new System.Drawing.Size(75, 23);
            this.four.TabIndex = 5;
            this.four.Text = "4";
            this.four.UseVisualStyleBackColor = true;
            this.four.Click += new System.EventHandler(this.four_Click);
            // 
            // seven
            // 
            this.seven.Location = new System.Drawing.Point(12, 101);
            this.seven.Name = "seven";
            this.seven.Size = new System.Drawing.Size(75, 23);
            this.seven.TabIndex = 6;
            this.seven.Text = "7";
            this.seven.UseVisualStyleBackColor = true;
            this.seven.Click += new System.EventHandler(this.seven_Click);
            // 
            // eight
            // 
            this.eight.Location = new System.Drawing.Point(93, 101);
            this.eight.Name = "eight";
            this.eight.Size = new System.Drawing.Size(75, 23);
            this.eight.TabIndex = 7;
            this.eight.Text = "8";
            this.eight.UseVisualStyleBackColor = true;
            this.eight.Click += new System.EventHandler(this.eight_Click);
            // 
            // nine
            // 
            this.nine.Location = new System.Drawing.Point(174, 101);
            this.nine.Name = "nine";
            this.nine.Size = new System.Drawing.Size(75, 23);
            this.nine.TabIndex = 8;
            this.nine.Text = "9";
            this.nine.UseVisualStyleBackColor = true;
            this.nine.Click += new System.EventHandler(this.nine_Click);
            // 
            // dodac
            // 
            this.dodac.Location = new System.Drawing.Point(264, 43);
            this.dodac.Name = "dodac";
            this.dodac.Size = new System.Drawing.Size(75, 23);
            this.dodac.TabIndex = 9;
            this.dodac.Text = "+";
            this.dodac.UseVisualStyleBackColor = true;
            this.dodac.Click += new System.EventHandler(this.dodac_Click);
            // 
            // odjac
            // 
            this.odjac.Location = new System.Drawing.Point(264, 72);
            this.odjac.Name = "odjac";
            this.odjac.Size = new System.Drawing.Size(75, 23);
            this.odjac.TabIndex = 10;
            this.odjac.Text = "-";
            this.odjac.UseVisualStyleBackColor = true;
            this.odjac.Click += new System.EventHandler(this.odjac_Click);
            // 
            // podzielic
            // 
            this.podzielic.Location = new System.Drawing.Point(264, 101);
            this.podzielic.Name = "podzielic";
            this.podzielic.Size = new System.Drawing.Size(75, 23);
            this.podzielic.TabIndex = 11;
            this.podzielic.Text = "/";
            this.podzielic.UseVisualStyleBackColor = true;
            this.podzielic.Click += new System.EventHandler(this.podzielic_Click);
            // 
            // mnożenie
            // 
            this.mnożenie.Location = new System.Drawing.Point(264, 130);
            this.mnożenie.Name = "mnożenie";
            this.mnożenie.Size = new System.Drawing.Size(75, 23);
            this.mnożenie.TabIndex = 12;
            this.mnożenie.Text = "*";
            this.mnożenie.UseVisualStyleBackColor = true;
            this.mnożenie.Click += new System.EventHandler(this.mnożenie_Click);
            // 
            // podsumowanie
            // 
            this.podsumowanie.Location = new System.Drawing.Point(174, 130);
            this.podsumowanie.Name = "podsumowanie";
            this.podsumowanie.Size = new System.Drawing.Size(75, 23);
            this.podsumowanie.TabIndex = 13;
            this.podsumowanie.Text = "=";
            this.podsumowanie.UseVisualStyleBackColor = true;
            this.podsumowanie.Click += new System.EventHandler(this.podsumowanie_Click);
            // 
            // wynik
            // 
            this.wynik.Location = new System.Drawing.Point(12, 12);
            this.wynik.MaxLength = 12512;
            this.wynik.Name = "wynik";
            this.wynik.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.wynik.Size = new System.Drawing.Size(327, 20);
            this.wynik.TabIndex = 14;
            // 
            // zero
            // 
            this.zero.Location = new System.Drawing.Point(12, 130);
            this.zero.Name = "zero";
            this.zero.Size = new System.Drawing.Size(75, 23);
            this.zero.TabIndex = 15;
            this.zero.Text = "0";
            this.zero.UseVisualStyleBackColor = true;
            this.zero.Click += new System.EventHandler(this.zero_Click);
            // 
            // clear
            // 
            this.clear.Location = new System.Drawing.Point(93, 130);
            this.clear.Name = "clear";
            this.clear.Size = new System.Drawing.Size(75, 23);
            this.clear.TabIndex = 16;
            this.clear.Text = "C";
            this.clear.UseVisualStyleBackColor = true;
            this.clear.Click += new System.EventHandler(this.clear_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 157);
            this.Controls.Add(this.clear);
            this.Controls.Add(this.zero);
            this.Controls.Add(this.wynik);
            this.Controls.Add(this.podsumowanie);
            this.Controls.Add(this.mnożenie);
            this.Controls.Add(this.podzielic);
            this.Controls.Add(this.odjac);
            this.Controls.Add(this.dodac);
            this.Controls.Add(this.nine);
            this.Controls.Add(this.eight);
            this.Controls.Add(this.seven);
            this.Controls.Add(this.four);
            this.Controls.Add(this.five);
            this.Controls.Add(this.six);
            this.Controls.Add(this.three);
            this.Controls.Add(this.two);
            this.Controls.Add(this.one);
            this.Name = "Form1";
            this.Text = "KALKULATOR DLA ARCHERA";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button one;
        private System.Windows.Forms.Button two;
        private System.Windows.Forms.Button three;
        private System.Windows.Forms.Button six;
        private System.Windows.Forms.Button five;
        private System.Windows.Forms.Button four;
        private System.Windows.Forms.Button seven;
        private System.Windows.Forms.Button eight;
        private System.Windows.Forms.Button nine;
        private System.Windows.Forms.Button dodac;
        private System.Windows.Forms.Button odjac;
        private System.Windows.Forms.Button podzielic;
        private System.Windows.Forms.Button mnożenie;
        private System.Windows.Forms.Button podsumowanie;
        private System.Windows.Forms.TextBox wynik;
        private System.Windows.Forms.Button zero;
        private System.Windows.Forms.Button clear;
    }
}

